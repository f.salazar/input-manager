######################################################
# Syntax Coloring Map For Input Manager Library
######################################################

#######################################
# Datatypes (KEYWORD1)
#######################################

INPUTMANAGER	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

setupInput KEYWORD2
modeInputLP KEYWORD2
handlerISR	KEYWORD2
loopInputsPolling	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################