#include <Arduino.h>
#include <InputManager.h>

InputManager im;

void input1Handler()
{
  im.handlerISR(1, [](byte event)
                { Serial.println("input: 1 event received: " + String(event)); });
}

void input2Handler()
{
  im.handlerISR(2, [](byte event)
                { Serial.println("input: 2 event received: " + String(event)); });
}

void setup()
{
  Serial.begin(115200);
  im.setupInput(1, 32, INPUT_PULLDOWN, input1Handler);
  im.modeInputLP(1, true, 2000);
  im.setupInput(2, 33, INPUT_PULLDOWN, input2Handler);
  im.modeInputLP(2, true, 2000);
  Serial.println("Boot done");
}

void loop()
{
  vTaskDelete(NULL);
  vTaskDelay(10);
}