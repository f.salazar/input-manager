#include <Arduino.h>
#include <InputManager.h>

InputManager im;

void inputAction(byte input, byte event)
{
  Serial.println("input: " + String(input) + " event received: " + String(event));
}

void setup()
{
  Serial.begin(115200);
  im.setupInput(1, 32, inputAction, INPUT_PULLDOWN);
  im.modeInputLP(1, true, 3000);
  im.setupInput(2, 33, inputAction, INPUT_PULLDOWN);
  im.modeInputLP(2, true, 3000);
  Serial.println("Boot done");
}

void loop()
{
  im.loopInputsPolling();
}

// input: 2 event received: 0
// input: 1 event received: 0
// input: 2 event received: 0
// input: 1 event received: 1
// input: 2 event received: 1