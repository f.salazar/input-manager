#include "InputManager.h"

int InputManager::SHORT_PRESS_FILTER_TIME = 80;
int InputManager::POLLING_FILTER_TIME = 20;
int InputManager::INPUT_POLLING_COUNT;
int InputManager::SHORT_PRESS_RESET_TIME = 300;
unsigned long InputManager::_poolingPrevious = millis();

InputManager::InputConfig_t InputManager::_inputConfig[10];

InputManager::InputManager(uint8_t inputs)
{
  INPUT_POLLING_COUNT = inputs;
}

int InputManager::_getInputMode(byte input)
{
  return _inputConfig[input].inputMode;
}

int InputManager::_getModeEvaluation(byte input)
{
  int evaluation = 0;
  int inputMode = _getInputMode(input);
  if (inputMode == INPUT || inputMode == INPUT_PULLDOWN)
    evaluation = 1;
  return evaluation;
}

int InputManager::_readPinStatus(byte input)
{
  int pin = _inputConfig[input].pin;
  return digitalRead(pin);
}

void InputManager::_handlerISR(byte input, void (*action)(byte event))
{
  if (!_inputConfig[input].enabled)
    return;
  byte evaluation = _getModeEvaluation(input);
  byte read = _readPinStatus(input);
  unsigned long currentTime = millis();
  unsigned long startTime = _inputConfig[input].startTime;

  if (read == evaluation)
  {
    if (!_inputConfig[input].status)
    {
      if (currentTime - startTime < SHORT_PRESS_FILTER_TIME)
        return;
      _inputConfig[input].startTime = millis();
      _inputConfig[input].status = true;
    }
  }
  else if (read == !evaluation)
  {
    if (_inputConfig[input].status)
    {
      _inputConfig[input].status = false;
      unsigned long lpTargetTime = _inputConfig[input].lpPressTime;
      if (currentTime - startTime > lpTargetTime)
      {
        if (_inputConfig[input].lpStatus)
          action(LONG);
      }
      else
      {
        action(SHORT);
      }
    }
  }
}

void InputManager::_handlerPooling(byte input)
{
  if (!_inputConfig[input].enabled)
    return;
  byte evaluation = _getModeEvaluation(input);
  byte read = _readPinStatus(input);
  unsigned long currentTime = millis();

  if (read == evaluation)
  {
    if (!_inputConfig[input].status)
    {
      if ((currentTime - _inputConfig[input].startTime) < SHORT_PRESS_FILTER_TIME)
        return;
      _inputConfig[input].startTime = millis();
      _inputConfig[input].status = true;
    }
    if (!_inputConfig[input].available)
    {
      _inputConfig[input].available = true;
    }
    _longPressPolling(input);
  }
  else if (read == !evaluation && _inputConfig[input].status)
  {
    if (!_inputConfig[input].reset)
    {
      _inputConfig[input].resetTime = millis();
      _inputConfig[input].reset = true;
      if (_inputConfig[input].available && !_inputConfig[input].longPress && !_inputConfig[input].shortPress)
      {
        _inputConfig[input].actionPolling(_inputConfig[input].input, SHORT);
        _inputConfig[input].shortPress = true;
      }
    }

    if ((currentTime - _inputConfig[input].resetTime) > SHORT_PRESS_RESET_TIME && _inputConfig[input].reset)
    {
      _inputConfig[input].reset = false;
      _inputConfig[input].longPress = false;
      _inputConfig[input].available = false;
      _inputConfig[input].status = false;
      _inputConfig[input].shortPress = false;
    }
  }
}

void InputManager::_longPressPolling(byte input)
{
  unsigned long currentTime = millis();
  unsigned long lpTargetTime = _inputConfig[input].lpPressTime;
  unsigned long startTime = _inputConfig[input].startTime;

  if (_inputConfig[input].lpStatus && (currentTime - startTime) >= lpTargetTime)
  {
    if (!_inputConfig[input].longPress)
    {
      _inputConfig[input].actionPolling(_inputConfig[input].input, LONG);
      _inputConfig[input].longPress = true;
    }
  }
}

void InputManager::setupInput(byte input, byte pin, void (*actionPolling)(byte input, byte event), int inputMode)
{
  _inputConfig[input].input = input;
  _inputConfig[input].pin = pin;
  _inputConfig[input].inputMode = inputMode;
  _inputConfig[input].actionPolling = actionPolling;
  enableInput(input);
  pinMode(pin, inputMode);
}

void InputManager::setupInput(byte input, byte pin, int inputMode, void (*handler)(), int interruptMode)
{
  _inputConfig[input].input = input;
  _inputConfig[input].pin = pin;
  _inputConfig[input].inputMode = inputMode; // INPUT
  enableInput(input);
  pinMode(pin, inputMode);
  attachInterrupt(digitalPinToInterrupt(pin), handler, interruptMode);
}

void InputManager::modeInputLP(byte input, bool lpStatus, int lpTime)
{
  _inputConfig[input].lpStatus = lpStatus;
  _inputConfig[input].lpPressTime = lpTime;
}

void InputManager::disableInput(byte input)
{
  _inputConfig[input].enabled = false;
}

void InputManager::enableInput(byte input)
{
  _inputConfig[input].enabled = true;
}

void InputManager::handlerISR(byte input, void (*action)(byte event))
{
  _handlerISR(input, action);
}

byte InputManager::readInputState(byte input)
{
  int pin = _inputConfig[input].pin;
  return digitalRead(pin);
}

void InputManager::loopInputsPolling()
{
  unsigned long current = millis();

  if (current - _poolingPrevious >= POLLING_FILTER_TIME)
  {
    for (size_t i = 1; i <= INPUT_POLLING_COUNT; i++)
    {
      _handlerPooling(i);
    }
    
    _poolingPrevious = current;
  }
}