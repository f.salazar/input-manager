#ifndef _INPUT_MANAGER_H
#define _INPUT_MANAGER_H

#include "Arduino.h"

enum InputEvent
{
  SHORT,
  LONG
};

class InputManager
{
  typedef struct InputConfig
  {
    bool enabled = true;
    byte pin;
    byte input;
    int inputMode;
    uint16_t lpPressTime = 2000;
    bool status = false;
    byte lpStatus = false;
    unsigned long startTime = millis();
    byte available = false;
    byte reset = false;
    byte longPress = false;
    byte shortPress = false;
    unsigned long resetTime = millis();
    void (*actionPolling)(byte input, byte event);
  } InputConfig_t;

private:
  static int SHORT_PRESS_FILTER_TIME;
  static int SHORT_PRESS_RESET_TIME;
  static int POLLING_FILTER_TIME;
  static int INPUT_POLLING_COUNT;
  static int _getInputMode(byte input);
  static int _getModeEvaluation(byte input);
  static int _readPinStatus(byte input);
  static void _handlerISR(byte input, void (*action)(byte event));
  static void _handlerPooling(byte input);
  static void _longPressPolling(byte input);
  static unsigned long _poolingPrevious;
  static InputConfig_t _inputConfig[10];

public:
  InputManager(uint8_t inputs = 1);
  void setupInput(
      byte input, byte pin, void (*actionPolling)(byte input, byte event) = [](byte input, byte event)
                            { Serial.println("input: " + String(input) + " event received: " + String(event)); },
      int inputMode = INPUT);                                                                      // POLLING SETUP
  void setupInput(byte input, byte pin, int inputMode, void (*isr)(), int interruptMode = CHANGE); // ISR SETUP
  void modeInputLP(byte input, bool lpStatus = false, int lpTime = 2000);
  void handlerISR(
      byte input, void (*action)(byte event) = [](byte event)
                  { Serial.println("Action received: " + String(event)); });
  byte readInputState(byte input);
  void disableInput(byte input);
  void enableInput(byte input);
  void loopInputsPolling();
};

#endif