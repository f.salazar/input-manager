# Methods for use library

| Library | Description | Information |
| ---| --- | --- |
| [interrupt](https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt) | This is the easiest way to use inputs with support to short and long press | long press only work after button is released
| [polling]| This is another method to use inputs with support to short and long press too, but need one function into loop() for work unlike interrupt don't need | need loop for work |


# Input Manager library for Arduino/ESP32

Input manager library for Arduino or ESP32 for control with attachInterrupt or polling method, with easy functions and pass callback to receive events, with support to short press and long press events.

## Requirements

The following are needed 

* [Arduino](http://www.arduino.cc) or [ESP32](https://www.espressif.com/en/products/socs/esp32)
* Connects cable between GND or 3.3/5V (ESP32 use 3.3v max, arduino have 5v support into inputs)to pin and configure library for use with PULL_DOWN, INPUT or PULL_UP to Arduino/ESP32 if you want to use interrupt find pins compatible with attachInterrupt (case ESP32 any pin has compatible)

## Installation 

Create a folder named Wiegand in Arduino's libraries folder.  You will have the following folder structure:

	cd arduino/libraries
	git clone https://gitlab.com/f.salazar/input-manager.git InputManager


*This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.*

*This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.*
